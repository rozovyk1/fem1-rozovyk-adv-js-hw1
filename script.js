// define the Hamburger class

/**
 * Класс, объекты которого описывают параметры гамбургера.
 *
 * @constructor
 * @param size        Размер
 * @param stuffing    Начинка
 * @throws {HamburgerException}  При неправильном использовании
 */
let Hamburger = function (size, stuffing) {

    if (
        Object.is(size, Hamburger.SIZE_SMALL) ||
        Object.is(size, Hamburger.SIZE_LARGE)
    ) {
        this.size = size;
    } else {
        throw new HamburgerException("Такого розміру не існує");
    }

    if (
        Object.is(stuffing, Hamburger.STUFFING_CHEESE) ||
        Object.is(stuffing, Hamburger.STUFFING_SALAD) ||
        Object.is(stuffing, Hamburger.STUFFING_POTATO)
    ) {
        this.stuffing = stuffing;
    } else {
        throw new HamburgerException("Такого стафінгу не існує");
    }
    this.topping = [];
};

/* Размеры, виды начинок и добавок */
Hamburger.SIZE_SMALL = {
    price: 50,
    calories: 20,
};

Hamburger.SIZE_LARGE = {
    price: 100,
    calories: 40,
};

Hamburger.STUFFING_CHEESE = {
    price: 10,
    calories: 20,
};

Hamburger.STUFFING_SALAD = {
    price: 20,
    calories: 5,
};

Hamburger.STUFFING_POTATO = {
    price: 15,
    calories: 10,
};

Hamburger.TOPPING_MAYO = {
    price: 20,
    calories: 5,
};

Hamburger.TOPPING_SPICE = {
    price: 15,
    calories: 0,
};


/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerException (message) {
    this.message = message;
    this.name = "Исключение, определенное пользователем";
}


/**
 * Добавить добавку к гамбургеру. Можно добавить несколько
 * добавок, при условии, что они разные.
 *
 * @param topping     Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.addTopping = function (topping) {
    if (this.topping.includes(topping)) {
        throw new HamburgerException("Заборонено додавати два топінги");
    }
    this.topping.push(topping);
}

/**
 * Убрать добавку, при условии, что она ранее была
 * добавлена.
 *
 * @param topping   Тип добавки
 * @throws {HamburgerException}  При неправильном использовании
 */
Hamburger.prototype.removeTopping = function (topping) {
    let index = this.topping.indexOf(topping);
    if (index !== -1) {
        this.topping.splice(index, 1);
    } else {
        throw new HamburgerException("Такого топінгу не існує");
    }
}

/**
 * Получить список добавок.
 *
 * @return {Array} Массив добавленных добавок, содержит константы
 *                 Hamburger.TOPPING_*
 */
Hamburger.prototype.getToppings = function () {

}



// console.log(Hamburger.STUFFING_SALAD.price);
// console.log(Hamburger.TOPPING_SPICE.calories);
//
// Hamburger.prototype.sayHello = function() {
//     console.log("Hello, I'm " + this.size + this.stuffing);
// };

let h = new Hamburger(
    Hamburger.SIZE_SMALL,
    Hamburger.STUFFING_SALAD
);

console.log(h);


//
// let person2 = new Hamburger("Bob,", " я не гамбургін, я Боб");
// let person3 = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_SALAD);
//
// person1.sayHello();
// person2.sayHello();
// person3.sayHello();
//
// Hamburger.prototype.getPrice = function () {
//     return this.size
// };
//
// person3.getPrice();

// Hamburger.prototype.addTopping = function (topping) {
//     if (this.topping === topping) {
//         alert('double topping? r u sure?')
//     } else {
//         this.topping = topping
//     }
// };


//
// let smallPotato = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_POTATO);
// console.log(smallPotato);
//
// smallPotato.sayHello = function () {
//     let getPrice =
//     this.price = size.price + stuffing.price + topping.price;
// };


//
// let smallPotato = new Hamburger(burger.size.SIZE_SMALL, burger.stuffing.STUFFING_POTATO);
// console.log(smallPotato);
//
//

//
// let test = new Hamburger(burger.size.SIZE_SMALL, burger.stuffing.STUFFING_POTATO);
// test.addTopping('TOPPING_SPICE');
//
//     this.price = size.price + stuffing.price + topping.price;
//     this.calories = size.calories + stuffing.calories + topping.calories;
// // };
//
// var Person = function (firstName) {
//     this.firstName = firstName;
// };
//
// Person.prototype.sayHello = function() {
//     console.log("Hello, I'm " + this.firstName);
// };
//
// var person1 = new Person("Alice");
// var person2 = new Person("Bob");
// var helloFunction = person1.sayHello;
//
// // выведет "Hello, I'm Alice"
// person1.sayHello();
//
// // выведет "Hello, I'm Bob"
// person2.sayHello();
//
// // выведет "Hello, I'm undefined" (or fails
// // with a TypeError in strict mode)
// helloFunction();
//
// // выведет true
// console.log(helloFunction === person1.sayHello);
//
// // выведет true
// console.log(helloFunction === Person.prototype.sayHello);
//
// // выведет "Hello, I'm Alice"
// helloFunction.call(person1);
